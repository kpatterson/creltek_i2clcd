#ifndef LiquidCrystal_I2C_Creltek_h
#define LiquidCrystal_I2C_Creltek_h
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-28.
//
// @license
// Creltek::i2c_LCD by Kevin H. Patterson is licensed under a Creative Commons
// Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_i2clcd
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file LiquidCrystal_I2C_Creltek.h
// This file extends the Arduino "new_liquidcrystal" library to support the
// Creltek i2c LCD Backpack, attached to a HD44780-compatible display.
//
// @dependencies
// Standard Arduino libraries
// "Creltek::i2cLCD" (included)
// "new_liquidcrystal" library:
//   https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
//
// @brief
// This file extends the Arduino "new_liquidcrystal" library to support the
// Creltek i2c LCD Backpack, attached to a HD44780-compatible display.
//
// This class must be constructed with a reference to a previously-constructed
// Creltek::i2cLCD object. (This i2cLCD object can also be directly used to
// access advanced features such as contrast setting, backlight enable,
// backlight RGB color setting, and GPIO.)
//
// This class only provides the virtual method implementations necessary for
// the superclass. For full interface documentation, refer to the superclass
// "LCD" in the "new_liquidcrystal" library.
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#include <LCD.h>

#include <inttypes.h>
#include <Print.h>


namespace Creltek {
	class i2cLCD;
}


class LiquidCrystal_I2C_Creltek
: public LCD
{
public:
	/*!
	 @method
	 @abstract   Class constructor.
	 @discussion Initializes class variables.
	 @param      i_LCD[in] You must pass a reference to a previously-constructed
	 Creltek::i2cLCD object.
	 */
	LiquidCrystal_I2C_Creltek( Creltek::i2cLCD& i_LCD );

	virtual ~LiquidCrystal_I2C_Creltek() {}

	/*!
	 @function
	 @abstract   Send a particular value to the LCD.
	 @discussion Sends a particular value to the LCD for writing to the LCD or
	 as an LCD command.

	 Users should never call this method. Use superclass methods instead.

	 @param      value[in] Value to send to the LCD.
	 @param      mode[in] DATA: write a CGRAM to the LCD; COMMAND - write a
	 command to the LCD.
	 */
	virtual void send( uint8_t value, uint8_t mode );

protected:
	Creltek::i2cLCD& m_LCD;
};

#endif
