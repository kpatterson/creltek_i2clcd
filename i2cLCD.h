#ifndef _Creltek_i2cLCD_h_
#define _Creltek_i2cLCD_h_
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-28.
//
// @license
// Creltek::i2cLCD by Kevin H. Patterson is licensed under a Creative
// Commons Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_i2clcd
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file i2cLCD.h
// This class provides a full-featured driver for the Creltek i2c LCD
// Backpack. It can used to access advanced features such as contrast setting,
// backlight enable, backlight RGB color setting, and GPIO, as well as sending
// data and commands to the attached LCD display.
//
// @dependencies
// Standard Arduino libraries
// Creltek::MCP23017
//   https://bitbucket.org/kpatterson/creltek_mcp23017
// Creltek::MCP4xxx
//   https://bitbucket.org/kpatterson/creltek_mcp4xxx
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#include <MCP23017.h>
#include <MCP4xxx.h>

#include <inttypes.h>

namespace Creltek {

// these values have been imperically determined for a generic thin-film 20 mA x RGB LED

#define R_m 105
#define R_b 11974
#define G_m 227   // thin-film "true Grn"
#define G_b 18376
#define B_m 184
#define B_b 17935

// these values have been imperically determined for generic standard 20 mA R, G, and B LEDs

//#define R_m 106     // generic standard red LED
//#define R_b 11312
//
//#define G_m 118     // generic standard green LED
//#define G_b 12113
//
//#define B_m 209     // generic standard blue LED
//#define B_b 20010
//
//#define R_m 107     // modern Lite-On standard red LED
//#define R_b 11946
//
//#define B_m 172     // modern Cree stardard blue LED
//#define B_b 18342


#define BitsMask_Monochrome 0b00001000

#define BitsMask_RGBZero 0b00000111

#define BitsMask_RedZero 0b00000001
#define BitsMask_GrnZero 0b00000010
#define BitsMask_BluZero 0b00000100

#define BitsMask_RGBDisabled 0b01110000

#define BitsMask_RedDisabled 0b00010000
#define BitsMask_GrnDisabled 0b00100000
#define BitsMask_BluDisabled 0b01000000

#define GPIOMask_RGBDisabled 0b00000111

#define GPIOMask_GPA3 0b00001000
#define GPIOMask_GPA4 0b00010000

#define GPIOMask_RS 0b00100000
#define GPIOMask_RW 0b01000000
#define GPIOMask_EN 0b10000000


	class i2cLCD {
	public:
		i2cLCD( uint8_t i_GPIOAddress = 0b0000111, uint8_t i_PotsAddress = 0b0000110 )
		: m_GPIO( i_GPIOAddress )
		, m_Pot0( i_PotsAddress & 0b1111110 )
		, m_Pot1( (i_PotsAddress & 0b1111110) | 0b0000001 )
		, m_Bits( 0 )
		{}

		void init() {
			m_GPIO.init();
			m_GPIO.write( 1, 0 );
			m_GPIO.write( 0, 0 );
			DisableBacklight();
			m_Pot0.init();
			m_Pot1.init();
			SetBacklightCurrent( 200 ); // 20.0 mA
			SetContrast( 240 ); // (255 = 0V or minimum resistance to GND)
		}

		// Backlight methods

		inline void DisableBacklight() { EnableBacklightRGB( false ); }

		inline void EnableBacklightMonochrome( bool i_Enabled = true ) {
			EnableBacklight( i_Enabled, false, false );
		}

		inline void EnableBacklightRGB( bool i_Enabled = true ) {
			EnableBacklight( i_Enabled, i_Enabled, i_Enabled );
		}

		void EnableBacklight( bool i_RedEnabled, bool i_GrnEnabled, bool i_BluEnabled ) {
			const bool Monochrome = i_RedEnabled && !(i_GrnEnabled || i_BluEnabled);
			m_Bits = (m_Bits & ~(BitsMask_Monochrome | BitsMask_RGBDisabled)) | (BitsMask_Monochrome * Monochrome) | (!i_RedEnabled * BitsMask_RedDisabled) | (!i_GrnEnabled * BitsMask_GrnDisabled) | (!i_BluEnabled * BitsMask_BluDisabled);
			p_WriteBacklightBits();
		}

		uint8_t GetBacklightCurrent( uint8_t i_Channel ) {
			switch( i_Channel ) {
				case 0: {
					return m_RedCurrent;
				} break;
				case 1: {
					return m_GrnCurrent;
				} break;
				case 2: {
					return m_BluCurrent;
				} break;
			}
			return 0;
		}

		// Current is in 1/10 mA (i.e. 150 = 15.0 mA)
		uint8_t SetBacklightCurrent( uint8_t i_Current ) {
			const bool ZeroCurrent = (i_Current == 0);
			if( (ZeroCurrent * BitsMask_RGBZero) != (m_Bits & BitsMask_RGBZero) ) {
				m_Bits = (m_Bits & ~BitsMask_RGBZero) | (ZeroCurrent * BitsMask_RGBZero);
				p_WriteBacklightBits();
			}
			uint8_t Current = SetBacklightCurrentRed( i_Current );
			if( !(m_Bits & BitsMask_Monochrome) ) {
				SetBacklightCurrentGrn( i_Current );
				SetBacklightCurrentBlu( i_Current );
			}
			return Current;
		}

		uint8_t SetBacklightCurrent( uint8_t i_Channel, uint8_t i_Current ) {
			switch( i_Channel ) {
				case 0: {
					return SetBacklightCurrentRed( i_Current );
				} break;
				case 1: {
					return SetBacklightCurrentGrn( i_Current );
				} break;
				case 2: {
					return SetBacklightCurrentBlu( i_Current );
				} break;
			}
			return 0;
		}

		void SetBacklightCurrentRGB( uint8_t i_RedCurrent, uint8_t i_GrnCurrent, uint8_t i_BluCurrent ) {
			const uint8_t OldBits = m_Bits;
			m_Bits = (OldBits & ~BitsMask_RGBZero) | ((i_RedCurrent == 0) * BitsMask_RedZero) | ((i_GrnCurrent == 0) * BitsMask_GrnZero) | ((i_BluCurrent == 0) * BitsMask_BluZero);
			if( m_Bits != OldBits )
				p_WriteBacklightBits();

			p_SetBacklightCurrentRed( i_RedCurrent );
			if( !(m_Bits & BitsMask_Monochrome) ) {
				p_SetBacklightCurrentGrn( i_GrnCurrent );
				p_SetBacklightCurrentBlu( i_BluCurrent );
			}
		}

		// Current is in 1/10 mA (i.e. 150 = 15.0 mA)
		uint8_t SetBacklightCurrentRed( uint8_t i_Current ) {
			if( (i_Current == 0) != bool(m_Bits & BitsMask_RedZero) ) {
				m_Bits ^= BitsMask_RedZero;
				p_WriteBacklightBits();
			}
			if( i_Current != 0 )
				p_SetBacklightCurrentRed( i_Current );
			else
				m_RedCurrent = 0;

			return m_RedCurrent;
		}

		uint8_t SetBacklightCurrentGrn( uint8_t i_Current ) {
			if( m_Bits & BitsMask_Monochrome ) return 0;
			if( (i_Current == 0) != bool(m_Bits & BitsMask_GrnZero) ) {
				m_Bits ^= BitsMask_GrnZero;
				p_WriteBacklightBits();
			}
			if( i_Current != 0 )
				p_SetBacklightCurrentGrn( i_Current );
			else
				m_GrnCurrent = 0;

			return m_GrnCurrent;
		}

		uint8_t SetBacklightCurrentBlu( uint8_t i_Current ) {
			if( m_Bits & BitsMask_Monochrome ) return 0;
			if( (i_Current == 0) != bool(m_Bits & BitsMask_BluZero) ) {
				m_Bits ^= BitsMask_BluZero;
				p_WriteBacklightBits();
			}
			if( i_Current != 0 )
				p_SetBacklightCurrentBlu( i_Current );
			else
				m_BluCurrent = 0;

			return m_BluCurrent;
		}

		uint8_t GetBacklightVoltage( uint8_t i_Channel ) {
			switch( i_Channel ) {
				case 0: {
					return m_Pot0.quickRead( 0 );
				} break;
				case 1: {
					return m_Pot1.quickRead( 0 );
				} break;
				case 2: {
					return m_Pot1.quickRead( 1 );
				} break;
			}
			return 0;
		}

		inline uint8_t SetBacklightVoltage( uint8_t i_Voltage ) {
			SetBacklightVoltageRGB( i_Voltage, i_Voltage, i_Voltage );
			return i_Voltage;
		}

		void SetBacklightVoltageRGB( uint8_t i_RedVoltage, uint8_t i_GrnVoltage, uint8_t i_BluVoltage ) {
			if( m_Bits & BitsMask_RGBZero ) {
				m_Bits &= ~BitsMask_RGBZero;
				p_WriteBacklightBits();
			}
			p_SetBacklightVoltageRed( i_RedVoltage );
			if( !(m_Bits & BitsMask_Monochrome) ) {
				p_SetBacklightVoltageGrn( i_GrnVoltage );
				p_SetBacklightVoltageBlu( i_BluVoltage );
			}
		}

		uint8_t SetBacklightVoltageRed( uint8_t i_Voltage ) {
			if( m_Bits & BitsMask_RedZero ) {
				m_Bits &= ~BitsMask_RedZero;
				p_WriteBacklightBits();
			}
			p_SetBacklightVoltageRed( i_Voltage );
			return i_Voltage;
		}

		uint8_t SetBacklightVoltageGrn( uint8_t i_Voltage ) {
			if( m_Bits & BitsMask_Monochrome ) return 0;
			if( m_Bits & BitsMask_GrnZero ) {
				m_Bits &= ~BitsMask_GrnZero;
				p_WriteBacklightBits();
			}
			p_SetBacklightVoltageGrn( i_Voltage );
			return i_Voltage;
		}

		uint8_t SetBacklightVoltageBlu( uint8_t i_Voltage ) {
			if( m_Bits & BitsMask_Monochrome ) return 0;
			if( m_Bits & BitsMask_BluZero ) {
				m_Bits &= ~BitsMask_BluZero;
				p_WriteBacklightBits();
			}
			p_SetBacklightVoltageBlu( i_Voltage );
			return i_Voltage;
		}

		// Contrast methods

		inline uint8_t GetContrast() {
			int16_t n = m_Pot0.quickRead( 1 ) - 1;
			if( n < 0 ) n = 0;
			return n;
		}

		inline uint8_t SetContrast( uint8_t i_Contrast ) {
			m_Pot0.write( 1, i_Contrast + 1 );
			return i_Contrast;
		}

		// LCD I/O methods

		inline void WriteData( uint8_t i_Data ) {
			m_GPIO.write( 1, i_Data );
		}

		void WriteRS( bool i_Value ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~GPIOMask_RS) | (i_Value * GPIOMask_RS) );
		}

		void WriteRW( bool i_Value ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~GPIOMask_RW) | (i_Value * GPIOMask_RW) );
		}

		void WriteEN( bool i_Value ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~GPIOMask_EN) | (i_Value * GPIOMask_EN) );
		}

		void WritePulse( bool i_DataMode = true ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~(GPIOMask_RS | GPIOMask_RW)) | GPIOMask_EN | (i_DataMode * GPIOMask_RS) );
			m_GPIO.write( 0, m_GPIO.quickRead( 0 ) & ~(GPIOMask_EN) );
		}

		// Extra GPIO Methods

		void WriteGPA3( bool i_Value ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~GPIOMask_GPA3) | (i_Value * GPIOMask_GPA3) );
		}

		void WriteGPA4( bool i_Value ) {
			m_GPIO.write( 0, (m_GPIO.quickRead( 0 ) & ~GPIOMask_GPA4) | (i_Value * GPIOMask_GPA4) );
		}

		// Member Accessors

		MCP23017& GetGPIORef() { return m_GPIO; }
		MCP4xxx& GetPot0Ref() { return m_Pot0; }
		MCP4xxx& GetPot1Ref() { return m_Pot1; }

	private:
		void p_WriteBacklightBits() {
			uint8_t Bits = (m_GPIO.quickRead( 0 ) & ~GPIOMask_RGBDisabled) | (m_Bits & BitsMask_RGBZero) | ((m_Bits & BitsMask_RGBDisabled) >> 4);
			m_GPIO.write( 0, Bits );
		}

		void p_SetBacklightCurrentRed( uint8_t i_Current ) {
			const uint16_t r = (R_m * uint16_t( i_Current ) + R_b) >> 8;
			m_Pot0.write( 0, r );
			m_RedCurrent = i_Current;
		}

		void p_SetBacklightCurrentGrn( uint8_t i_Current ) {
			const uint16_t g = (G_m * uint16_t( i_Current ) + G_b) >> 8;
			m_Pot1.write( 0, g );
			m_GrnCurrent = i_Current;
		}
		
		void p_SetBacklightCurrentBlu( uint8_t i_Current ) {
			const uint16_t b = (B_m * uint16_t( i_Current ) + B_b) >> 8;
			m_Pot1.write( 1, b );
			m_BluCurrent = i_Current;
		}  
		
		inline void p_SetBacklightVoltageRed( uint8_t i_Voltage ) {
			m_Pot0.write( 0, i_Voltage );
		}
		
		inline void p_SetBacklightVoltageGrn( uint8_t i_Voltage ) {
			m_Pot1.write( 0, i_Voltage );
		}
		
		inline void p_SetBacklightVoltageBlu( uint8_t i_Voltage ) {
			m_Pot1.write( 1, i_Voltage );
		}
		
	private:
		MCP23017 m_GPIO;
		MCP4xxx m_Pot0;
		MCP4xxx m_Pot1;
		uint8_t m_Bits;
		uint8_t m_RedCurrent;
		uint8_t m_GrnCurrent;
		uint8_t m_BluCurrent;
	};

}

#endif // _Creltek_i2cLCD_h_
