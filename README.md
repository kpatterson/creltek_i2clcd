# README #

## Creltek::i2cLCD ##

The i2cLCD class provides a full-featured driver for the Creltek i2c LCD Backpack. It can used to access advanced features such as contrast setting, backlight enable, backlight RGB color setting, and GPIO, as well as sending data and commands to the attached LCD display.

## Creltek::LiquidCrystal_I2C_Creltek ##

The LiquidCrystal_I2C_Creltek class extends the Arduino "new_liquidcrystal" library to support the Creltek i2c LCD Backpack, attached to a HD44780-compatible display.

This class must be constructed with a reference to a previously-constructed Creltek::i2cLCD object. (This i2cLCD object can also be directly used to access advanced features such as contrast setting, backlight enable, backlight RGB color setting, and GPIO.)

This class only provides the virtual method implementations necessary for the superclass. For full interface documentation, refer to the superclass "LCD" in the "new_liquidcrystal" library.