// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-28.
//
// @license
// Creltek::i2c_LCD by Kevin H. Patterson is licensed under a Creative Commons
// Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_i2clcd
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file LiquidCrystal_I2C_Creltek.cpp
// This file extends the Arduino "new_liquidcrystal" library to support the
// Creltek i2c LCD Backpack, attached to a HD44780-compatible display.
//
// @dependencies
// Standard Arduino libraries
// "Creltek::i2cLCD" (included)
// "new_liquidcrystal" library:
//   https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
//
// @brief
// This file extends the Arduino "new_liquidcrystal" library to support the
// Creltek i2c LCD Backpack, attached to a HD44780-compatible display.
//
// This class must be constructed with a reference to a previously-constructed
// Creltek::i2cLCD object. (This i2cLCD object can also be directly used to
// access advanced features such as contrast setting, backlight enable,
// backlight RGB color setting, and GPIO.)
//
// This class only provides the virtual method implementations necessary for
// the superclass. For full interface documentation, refer to the superclass
// "LCD" in the "new_liquidcrystal" library.
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#if (ARDUINO <  100)
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#include "LiquidCrystal_I2C_Creltek.h"
#include "i2cLCD.h"

#include <inttypes.h>


LiquidCrystal_I2C_Creltek::LiquidCrystal_I2C_Creltek( Creltek::i2cLCD& i_LCD )
: m_LCD( i_LCD )
{
	_displayfunction = LCD_8BITMODE | LCD_1LINE | LCD_5x8DOTS;
	_polarity = POSITIVE;
}


void LiquidCrystal_I2C_Creltek::send( uint8_t value, uint8_t mode )
{
	//	m_LCD.WriteRS( mode == DATA );
	//	m_LCD.WriteRW( false );

	if( mode == FOUR_BITS )
		m_LCD.WriteData( (value & 0x0F) << 4 );
	else
		m_LCD.WriteData( value );

	//	m_LCD.WriteEN( true );   // En HIGH
	//	delayMicroseconds( 1 );
	//	m_LCD.WriteEN( false );  // En LOW

	m_LCD.WritePulse( mode == DATA );
	//	delayMicroseconds( 35 );
}
